// ==UserScript==
// @name         Duo 30.03.2021 Hide sentence icon and speech bubble
// @namespace    http://tampermonkey.net/
// @version      1.1
// @description  Adds CSS that hides the new icon and speech bubbles around Duo sentences
// @author       DTSFF
// @match        https://www.duolingo.com/*
// @grant        none
// ==/UserScript==

(() => {
  const CSS = `
    ._1KUxv._11rtD {
      border: none;
    }
    .ite_X {
      display: none;
    }
    .F2B9m {
      display: none;
    }
  `; // speech bubble border ; triangle ; image
  const headEl = document.querySelector('head');
  const newStyleEl = document.createElement('style');
  /* [MDN] There is very little reason to include the 'type' attribute in the <style>
   * element in modern web documents because "text/css" is actually the default and
   * only used value.
   */
  newStyleEl.appendChild(document.createTextNode(CSS));
  headEl.appendChild(newStyleEl);
})();
